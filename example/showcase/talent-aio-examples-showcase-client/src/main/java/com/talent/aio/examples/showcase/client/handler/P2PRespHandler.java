package com.talent.aio.examples.showcase.client.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talent.aio.common.ChannelContext;
import com.talent.aio.examples.showcase.common.ShowcasePacket;
import com.talent.aio.examples.showcase.common.ShowcaseSessionContext;
import com.talent.aio.examples.showcase.common.intf.AbsShowcaseBsHandler;
import com.talent.aio.examples.showcase.common.packets.P2PRespBody;

/**
 * @author tanyaowu 
 * 2017年3月27日 下午9:51:28
 */
public class P2PRespHandler extends AbsShowcaseBsHandler<P2PRespBody>
{
	private static Logger log = LoggerFactory.getLogger(P2PRespHandler.class);

	/**
	 * 
	 * @author: tanyaowu
	 */
	public P2PRespHandler()
	{
	}

	/**
	 * @param args
	 * @author: tanyaowu
	 */
	public static void main(String[] args)
	{

	}
	/** 
	 * @return
	 * @author: tanyaowu
	 */
	@Override
	public Class<P2PRespBody> bodyClass()
	{
		return P2PRespBody.class;
	}

	/** 
	 * @param packet
	 * @param bsBody
	 * @param channelContext
	 * @return
	 * @throws Exception
	 * @author: tanyaowu
	 */
	@Override
	public Object handler(ShowcasePacket packet, P2PRespBody bsBody, ChannelContext<ShowcaseSessionContext, ShowcasePacket, Object> channelContext) throws Exception
	{
		return null;
	}
}
