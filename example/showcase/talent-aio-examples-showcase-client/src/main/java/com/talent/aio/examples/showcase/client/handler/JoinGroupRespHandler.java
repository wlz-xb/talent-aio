package com.talent.aio.examples.showcase.client.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talent.aio.common.ChannelContext;
import com.talent.aio.examples.showcase.common.ShowcasePacket;
import com.talent.aio.examples.showcase.common.ShowcaseSessionContext;
import com.talent.aio.examples.showcase.common.intf.AbsShowcaseBsHandler;
import com.talent.aio.examples.showcase.common.packets.GroupMsgRespBody;
import com.talent.aio.examples.showcase.common.packets.JoinGroupRespBody;

/**
 * @author tanyaowu 
 * 2017年3月27日 下午9:51:28
 */
public class JoinGroupRespHandler extends AbsShowcaseBsHandler<JoinGroupRespBody>
{
	private static Logger log = LoggerFactory.getLogger(JoinGroupRespHandler.class);

	/**
	 * 
	 * @author: tanyaowu
	 */
	public JoinGroupRespHandler()
	{
	}

	/**
	 * @param args
	 * @author: tanyaowu
	 */
	public static void main(String[] args)
	{

	}
	/** 
	 * @return
	 * @author: tanyaowu
	 */
	@Override
	public Class<JoinGroupRespBody> bodyClass()
	{
		return JoinGroupRespBody.class;
	}

	/** 
	 * @param packet
	 * @param bsBody
	 * @param channelContext
	 * @return
	 * @throws Exception
	 * @author: tanyaowu
	 */
	@Override
	public Object handler(ShowcasePacket packet, JoinGroupRespBody bsBody, ChannelContext<ShowcaseSessionContext, ShowcasePacket, Object> channelContext) throws Exception
	{
		return null;
	}
}
