package com.talent.aio.examples.showcase.server;

import java.io.IOException;

import com.talent.aio.examples.showcase.common.ShowcasePacket;
import com.talent.aio.examples.showcase.common.ShowcaseSessionContext;
import com.talent.aio.server.AioServer;
import com.talent.aio.server.ServerGroupContext;
import com.talent.aio.server.intf.ServerAioHandler;
import com.talent.aio.server.intf.ServerAioListener;

/**
 * 
 * @author tanyaowu 
 * 2017年3月27日 上午12:16:31
 */
public class ShowcaseServerStarter
{
	static ServerGroupContext<ShowcaseSessionContext, ShowcasePacket, Object> serverGroupContext = null;
	static AioServer<ShowcaseSessionContext, ShowcasePacket, Object> aioServer = null; //可以为空
	static ServerAioHandler<ShowcaseSessionContext, ShowcasePacket, Object> aioHandler = null;
	static ServerAioListener<ShowcaseSessionContext, ShowcasePacket, Object> aioListener = new ShowcaseServerAioListener();
	static String serverIp = null;
	static int serverPort = com.talent.aio.examples.showcase.common.Const.PORT;

	public static void main(String[] args) throws IOException
	{
		aioHandler = new ShowcaseServerAioHandler();
		aioListener = null; //可以为空
		serverGroupContext = new ServerGroupContext<>(aioHandler, aioListener);
		aioServer = new AioServer<>(serverGroupContext);
		aioServer.start(serverIp, serverPort);
	}
}